from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.connection import Connection

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}


def run_module():
    modules_args = {'host': {'type': str, 'required': True}}
    result = {'changed': False, 'original_message': '', 'message': ''}
    module = AnsibleModule(modules_args)
    con  = Connection(module)

    result['message'] = str(module._options_context)
    # =module.
    if module.check_mode:
        module.exit_json(**result)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
