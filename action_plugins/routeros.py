#!/usr/bin/python
from ansible.module_utils.connection import Connection
from ansible.plugins.action import ActionBase
from ansible.playbook.play_context import PlayContext
from ansible.errors import AnsibleError, AnsibleOptionsError

import librouteros


class ActionModule(ActionBase):
    def get_connection(self, result):
        cont = self._play_context
        assert isinstance(cont, PlayContext)
        user = cont.remote_user
        if user is None:
            raise AnsibleError('Can`t connect user not set')

        password = cont.password
        if password is None:
            raise AnsibleError('Password not set')
        api = librouteros.connect(self._play_context.remote_addr, user, password)
        if api is None:
            raise AnsibleError('Can`t connect to host ' + self._connection.host + " by API")
        return api

    def set(self, api, results, args):
        params = dict()
        remove_others = False
        try:
            path = args['path']
            now_all = api(path + '/print')  # Load current configuration

            if 'remove_others' in args:
                remove_others = args['remove_others']

            items = args['values']
            if items is None:
                items = dict()

            for tname, tval in items.iteritems():
                now = None
                match_key, match_value = (None, None)
                action = None
                params.clear()

                if tname == "global":
                    now = now_all[0]
                else:
                    if '/' not in tname:
                        raise AnsibleOptionsError("Configuration name should be in format <key>/<value> eg name/vlan ")
                    (match_key, match_value) = str(tname).split('/', 1)
                    for n in now_all:
                        # print(str(n[match_key]) + "==" + match_value + " by " + match_key + " match "+str(str(n[match_key]) == match_value))
                        if str(n[match_key]) == match_value:  # match configuration
                            now = n
                            n['---found---'] = True
                            break

                # print("Value now:"+repr(now))
                if now is not None:
                    # print("Copy "+now[".id"]+" "+now['name']+ " "+repr(tval))
                    for k, v in tval.iteritems():
                        action = "set"
                        if k not in now or now[k] != v:  # copy differ or absent parameters
                            params[k] = v
                else:  # No such record. Will create it
                    params = tval
                    action = "add"
                    if match_key is not None:
                        params[match_key] = match_value
                print (action+" "+repr(params))

                if len(params) > 0:
                    if now is not None and '.id' in now:
                        params['.id'] = now['.id']
                    ret = api(path + "/" + action, **params)
                    results['ansible_facts'] = dict(set_results=ret)
                    results['changed'] = True

            if remove_others:
                for n in now_all:
                    if "---found---" not in n:
                        params = dict()
                        params[".id"] = n['.id']
                        ret = api(path + "/remove", **params)
                        results['ansible_facts'] = dict(remove_result=ret)
                        results['changed'] = True

            return results
        except KeyError, e:
            raise AnsibleOptionsError("Need required option " + repr(e.args))
        except librouteros.TrapError, e:
            raise AnsibleError(e.message + " " + " ".join(params.keys()))

    def run(self, tmp=None, task_vars=None):
        result = super(ActionModule, self).run(tmp, task_vars)

        api = self.get_connection(result)

        args = self._task.args
        action = "set"

        if 'path' not in args:
            raise AnsibleOptionsError("path must be set")

        if 'action' in args:
            action = args['action']
        path = args['path']

        if action == 'query':
            ret = api(path + '/print')
            result['msg'] = "Loaded"
            result['ansible_facts'] = dict(routeros_facts=ret)
        elif action == "run":
            ret = api(path)
            result['msg'] = "Started"
            result['ansible_facts'] = dict(routeros_runnned=ret)
        elif action == "set":
            return self.set(api, result, args)
        else:
            raise AnsibleOptionsError("Unknown action " + action)

        result['failed'] = False
        result['changed'] = False
        return result
